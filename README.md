Material Design theme
=============

Design
------

The theme is designed with these keywords in mind, mobile-first, minimalism, less is more or similar.
White space is considered a design element, and also provides space around links and content in general.

Layout
------

Material Design theme is responsive and adapts the layout to the viewing environment using media queries and fluid images.

Elgg css is extended by views/default/material_theme/css, containing the media queries.

Requirements
------------

Place Material Design theme at or near the bottom of the plugin list.
